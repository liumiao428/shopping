import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tabState: "index",
    list: [],
    total: 0,
    lista:[]
  },
  mutations: {

    index_tab(state, text) {
      state.tabState = text
    },
    tab_list(state, res) {
      state.list = res.Product
      console.log(state.list);
      state.total = res.total
    },
    add(state,listAlist){
      state.lista=listAlist
    }
  },
  actions: {
  },
  modules: {
  }
})
