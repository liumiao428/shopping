import request from "./request.js";

//   axios常见的配置选项：
// axios({
//  请求地址
// url: “请求地址”,
//  请求类型
// method: ‘get/post……’,
//  请求的根路径
// baseURL: “根路径”,
//  自定义的请求头
// headers: { "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"},
//  URL查询对象，有1个同类的属性 data:{}一般配合post使用。params:{} 配合get使用
// params: {},
//  超时设置
// timeout:5000,
// })


//线上接口
//轮播图数据
function getSwiper() {
  return request({
    url: "/resources/carousel",
    method: "POST"
  });
}

//手机
function getList(obj) {
  return request({
    url: "product/getPromoProduct",
    method: "POST",
    data: obj
  })
  
}

// //家电
function getList2(obj) {
  return request({
    url: "product/getHotProduct",
    method: "POST",
    data: obj
  })
}

// 分类页面头部数据
function getSortTab() {
  return request({
    url: "product/getCategory",
    method: 'POST'
  })
}

// 分类全部数据
function getSort(obj) {
  return request({
    url: "product/getAllProduct",
    method: "POST",
    data: obj
  })
}

// 分类其他数据
function getOther(obj) {
  return request({
    url: "product/getProductByCategory",
    method: "POST",
    data: obj
  })
}

// 关于我们的数据
function getMd() {
  return request({
    url: 'public/docs/README.md',
    method: "GET"
  })
}
// 跳转详情的数据
function skip(obj){
  return request({
    url:'product/getDetails',
    method: "POST",
    data:obj
  })
}
// 
// 跳转详情的数据
function carousel(obj){
  return request({
    url:'product/getDetailsPicture',
    method: "POST",
    data:obj
  })
}
// 点击注册的数据
function debark(obj){
  return request({
    url:'users/register',
    method:'POST',
    data:obj
  })
}
// 判断用户是否存在
function name(obj){
  return request({
    url:'users/findUserName',
    method:'POST',
    data:obj
  })
}
// 点击登陆的数据
function logina(obj){
  return request({
    url:'users/login',
    method:"POST",
    data:obj
  })
}
// 点击喜欢的数据
function love(obj){
  return request({
    url:'user/collect/addCollect',
    method:'POST',
    data:obj
  })
}
// 查看收藏的数据
function collect(obj){
  return request({
    url:'user/shoppingCart/getShoppingCart',
    method:'POST',
    data:obj
  })
}
// 收藏的数据
function collectA(obj){
  return request({
    url:'user/collect/getCollect',
    method:"POST",
    data:obj
  })
}
// 删除收藏的数据
//  
function collectADel(obj){
  return request({
    url:'user/collect/deleteCollect',
    method:'POST',
    data:obj
  })
}
// 点击加入购物车
function addShopping(obj){
  return request({
    url:'user/shoppingCart/addShoppingCart',
    method:'POST',
    data:obj
  })
}
// 渲染购物车
function drawing(obj){
  return request({
    url:'user/shoppingCart/getShoppingCart',
    method:'POST',
    data:obj
  })
}
// 修改购物车列表 
function revampShopping (obj){
  return request({
    url:'user/shoppingCart/updateShoppingCart',
    method:'POST',
    data:obj
  })
}
// 删除购物车内容
// 
function deiList(obj){
  return request({
    url:'user/shoppingCart/deleteShoppingCart',
    method:'POST',
    data:obj
  })
}
// 我的订单
function order(obj){
  return request({
    url:'user/order/addOrder',
    method:'POST',
    data:obj
  })
}
// 我的订单
function ordera(obj){
  return request({
    url:'user/order/getOrder',
    method:'POST',
    data:obj
  })
}
// 搜索
function GetSearch(obj){
  return request({
    url:'product/getProductBySearch',
    method:'POST',
    data:obj
  })
}
// {search: "12", currentPage: 1, pageSize: 15}

export { getSwiper,getList,getList2,getSortTab,getSort,getOther,
  getMd,skip,carousel,debark,name,logina,love,collect,collectA,
  collectADel,addShopping,drawing,revampShopping,deiList,order,ordera,GetSearch}
