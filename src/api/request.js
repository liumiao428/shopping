import Axios from "axios";

// 1.创建axios的实例
const instance = Axios.create({
    baseURL: process.env.VUE_APP_BASE_API,
    timeout: 5000,
});

// 配置请求拦截器-常见的就是统一加入请求头
instance.interceptors.request.use(
    config => {

        return config;
    },
    err => {
        console.log('发送失败');
        return err;
    }
);

instance.interceptors.response.use(
    response => {
        return response.data;
    },
    err => {
        if (err && err.response) {
            switch (err.response.status) {
                case 400:
                    err.message = "请求错误";
                    break;
                case 401:
                    err.message = "未授权的访问";
                    break;
            }
        }
        console.log('响应失败1');
        return err;

    }
);

export default instance


